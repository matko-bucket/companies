package com.matko.orb.api

import com.matko.orb.model.CompanyDetailWrapper
import com.matko.orb.model.CompanySimpleWrapper
import com.matko.orb.model.Industries
import com.matko.orb.util.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CompanyApiService {

    private val api = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CompanyApi::class.java)


    suspend fun searchCompanySimpleByNameAndCountry(
        name: String,
        country: String
    ): CompanySimpleWrapper {
        return api.searchCompanySimpleByNameAndCountry(Constants.API_KEY, name, country)
    }

    suspend fun fetchCompanyDetails(api_key: String, orb: String): CompanyDetailWrapper {
        return api.fetchCompanyDetail(api_key, orb)
    }

    suspend fun fetchCompanyDetailsV2(fetchUrl: String): CompanyDetailWrapper {
        return api.fetchCompanyDetailV2(fetchUrl)
    }

    suspend fun fetchIndustries(): List<Industries> {
        return api.fetchIndustries(Constants.API_KEY)
    }

    suspend fun searchCompanyByIndustry(limit: Int, industry: String): CompanySimpleWrapper {
        return api.searchCompanyByIndustry(Constants.API_KEY, limit, industry)
    }

    suspend fun searchCompanies(
        name: String,
        country: String,
        industry: String
    ): CompanySimpleWrapper {
        return api.searchCompanies(Constants.API_KEY, name, country, industry)
    }
}