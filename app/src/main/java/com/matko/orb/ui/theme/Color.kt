package com.matko.orb.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Orange500 = Color(0xffe65c00)
val Orange200 = Color(0xffff9933)
val DarkGray = Color(0xff1a1a1a)
val AlmostBlack = Color(0xff0d0d0d)
val LightGray = Color(0xfff2f2f2)
val DarkGreen = Color(0xff666633)
val LightGreen = Color(0xffddddbb)

