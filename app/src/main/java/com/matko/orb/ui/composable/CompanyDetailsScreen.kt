package com.matko.orb.ui.composable

import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material.icons.rounded.Place
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser
import com.google.maps.android.compose.*
import com.matko.orb.viewModel.DetailsViewModel
import com.matko.orb.R
import com.matko.orb.model.Comment
import com.matko.orb.model.CompanyDetail
import com.matko.orb.ui.theme.Teal200
import com.matko.orb.util.TimeStampToDateTimeConverter
import com.matko.orb.viewModel.DetailsViewModelV2
import java.lang.Exception

var viewModel: DetailsViewModelV2? = null
var firebaseUser: FirebaseUser? = null
var company: CompanyDetail? = null

@Composable
fun CompanyDetailsScreen(
    detailsViewModel: DetailsViewModelV2,
    context: Context
) {
    viewModel = detailsViewModel
    firebaseUser = detailsViewModel.currentUser.observeAsState(null).value
    company = detailsViewModel.company.observeAsState(null).value
//    val companyAttributes = detailsVewModel.companyAttributesTitle
    val loading = detailsViewModel.loading.observeAsState()
    val comments = detailsViewModel.comments.observeAsState(null)
    val scrollState: ScrollState = rememberScrollState()
//    val companyAttributes = detailsVewModel.attributes
    val companyAttributes = arrayListOf(
        stringResource(R.string.company_name_search_hint),
        stringResource(R.string.company_country_search_hint),
        stringResource(R.string.company_city),
        stringResource(R.string.address),
        stringResource(R.string.website),
        stringResource(R.string.phone),
        stringResource(R.string.email),
        stringResource(R.string.number_of_employees),
        stringResource(R.string.description),
        )

    Box(modifier = Modifier.fillMaxSize()) {
        loading.value?.let { CircularIndeterminateProgressBar(isDisplayed = it) }

        Column(
            modifier = Modifier
                .verticalScroll(scrollState)
                .padding(top = 4.dp, bottom = 100.dp, start = 4.dp, end = 4.dp),
            verticalArrangement = Arrangement.Top

        ) {
            company?.let {

                for (i in 0..companyAttributes.size) {

                    when (i) {
                        0 -> it.name?.let { it1 ->
                            DetailsCard(title = companyAttributes[i], value = it1)
                        }
                        1 -> it.country?.let { it1 ->
                            DetailsCard(title = companyAttributes[i], value = it1)
                        }
                        2 -> it.city?.let { it1 ->
                            DetailsCard(title = companyAttributes[i], value = it1)
                        }
                        3 -> it.address?.let { it1 ->
                            AddressCard(title = companyAttributes[i], address = it1)
                        }
                        4 -> it.website?.let { it1 ->
                            WebsiteCard(title = companyAttributes[i], website = it1)
                        }
                        5 -> it.phone?.let { it1 ->
                            PhoneCard(title = companyAttributes[i], phone = it1)
                        }
                        6 -> it.email?.let { it1 ->
                            EmailCard(title = companyAttributes[i], email = it1)
                        }
                        7 -> it.employees?.let { it1 ->
                            DetailsCard(title = companyAttributes[i], value = it1.toString())
                        }
                        8 -> it.description?.let { it1 ->
                            DetailsCard(title = companyAttributes[i], value = it1)
                        }
                        else -> {}
                    }
                }
                GoogleMapCard(it, context)

                comments.value?.let { comments ->
                    CommentsContainer(comments)
                }
            }
        }
    }

//    LazyColumn(contentPadding = PaddingValues(4.dp)) {
//        items(items = companyAttributes) { title ->
//            company.value?.let { companyDetail ->
//                DetailsCard(title, companyDetail)
//            }
//
//        }
//    }

}

private fun getLocation(context: Context, companyDetail: CompanyDetail): LatLng? {
    val geoCoder = Geocoder(context)
    var address = companyDetail.address
    if (!companyDetail.city.isNullOrEmpty()) {
        address += ", " + companyDetail.city
    }
    var p1: LatLng? = try {
        val addresses = geoCoder.getFromLocationName(address, 5)
        val location = addresses[0]
        LatLng(location.latitude, location.longitude)
    } catch (e: Exception) {
        null
    }
    if (p1 == null && companyDetail.latitude != null && companyDetail.longitude != null) {
        p1 = LatLng(companyDetail.latitude, companyDetail.longitude)
    }
    return p1
}

@Composable
private fun DetailsCard(title: String, value: String) {
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
    ) {
        SelectionContainer {
            Column(
                modifier = Modifier
                    .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            ) {
                Text(
                    text = title,
                    style = MaterialTheme.typography.subtitle1,
                )
                Text(
                    text = value,
                    style = MaterialTheme.typography.body1
                )
            }
        }
    }
}

@Composable
private fun AddressCard(title: String, address: String) {
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
            .clickable(onClick = { viewModel?.navigateToMapFragment?.value = true })
    ) {
        SelectionContainer {
            ConstraintLayout(
                modifier = Modifier
                    .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            ) {
                val (column, icon) = createRefs()
                Column(
                    modifier = Modifier
                        .layoutId("address_column")
                        .fillMaxWidth()
                        .constrainAs(column) {
                            end.linkTo(icon.start)
                            start.linkTo(parent.start)
                        }
                ) {
                    Text(
                        text = title,
                        style = MaterialTheme.typography.subtitle1
                    )
                    Text(
                        text = address,
                        style = MaterialTheme.typography.body1
                    )
                }
                Icon(
                    imageVector = Icons.Rounded.Place,
                    contentDescription = "open map",
                    tint = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier
                        .wrapContentSize()
                        .constrainAs(icon) {
                            end.linkTo(parent.end)
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                        }
                )
            }
        }
    }
}

@Composable
private fun WebsiteCard(title: String, website: String) {
    val context = LocalContext.current
    val intent = remember { Intent(Intent.ACTION_VIEW, Uri.parse(website)) }
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
            .clickable(onClick = { context.startActivity(intent) })
    ) {
        SelectionContainer {
            ConstraintLayout(
                modifier = Modifier
                    .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            ) {
                val (column, image) = createRefs()
                Column(
                    modifier = Modifier
                        .layoutId("website_column")
                        .fillMaxWidth()
                        .constrainAs(column) {
                            end.linkTo(image.start)
                            start.linkTo(parent.start)
                        }
                ) {
                    Text(
                        text = title,
                        style = MaterialTheme.typography.subtitle1
                    )
                    Text(
                        text = website,
                        style = MaterialTheme.typography.body1
                    )
                }
                Image(
                    painter = painterResource(id = R.drawable.ic_baseline_web_24),
                    contentDescription = "web icon",
                    modifier = Modifier
                        .wrapContentSize()
                        .constrainAs(image) {
                            end.linkTo(parent.end)
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                        }
                )
            }
        }
    }
}

@Composable
private fun PhoneCard(title: String, phone: String) {
    val context = LocalContext.current
    val intent = remember { Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phone")) }
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
            .clickable(onClick = { context.startActivity(intent) })
    ) {
        SelectionContainer {
            ConstraintLayout(
                modifier = Modifier
                    .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            ) {
                val (column, icon) = createRefs()

                Column(
                    modifier = Modifier
                        .layoutId("phone_dial_column")
                        .fillMaxWidth()
                        .constrainAs(column) {
                            end.linkTo(icon.start)
                            start.linkTo(parent.start)
                        }
                ) {
                    Text(
                        text = title,
                        style = MaterialTheme.typography.subtitle1,
                    )
                    Text(
                        text = phone,
                        style = MaterialTheme.typography.body1
                    )
                }
                Icon(
                    imageVector = Icons.Rounded.Phone,
                    contentDescription = "dial",
                    tint = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier
                        .wrapContentSize()
                        .constrainAs(icon) {
                            end.linkTo(parent.end)
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                        }
                )
            }

        }
    }
}

@Composable
private fun EmailCard(title: String, email: String) {
    val context = LocalContext.current
//    val email2 = "kk@kk.kk"
    val intent = remember {
        Intent(Intent.ACTION_SENDTO).apply {
            // The intent does not have a URI, so declare the "text/plain" MIME type
//            type = "text/plain"
//            putExtra(Intent.EXTRA_EMAIL, arrayOf(email2)) // recipients
//            putExtra(Intent.EXTRA_SUBJECT, "Email subject")
//            putExtra(Intent.EXTRA_TEXT, "Email message text")
//            putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"))
            // You can also attach multiple items by passing an ArrayList of Uris
        }
    }
    intent.data = Uri.parse("mailto:$email")
    val chooser = Intent.createChooser(intent, stringResource(id = R.string.send_email_with))

    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
            .clickable(onClick = { context.startActivity(chooser) })
    ) {
        SelectionContainer {
            ConstraintLayout(
                modifier = Modifier
                    .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            ) {
                val (column, image) = createRefs()
                Column(
                    modifier = Modifier
                        .layoutId("email_column")
                        .fillMaxWidth()
                        .constrainAs(column) {
                            end.linkTo(image.start)
                            start.linkTo(parent.start)
                        }
                ) {
                    Text(
                        text = title,
                        style = MaterialTheme.typography.subtitle1
                    )
                    Text(
                        text = email,
                        style = MaterialTheme.typography.body1
                    )
                }
                Icon(
                    imageVector = Icons.Default.Email,
                    contentDescription = "email icon",
                    modifier = Modifier
                        .wrapContentSize()
                        .constrainAs(image) {
                            end.linkTo(parent.end)
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                        }
                )
            }
        }
    }
}

@Composable
private fun GoogleMapCard(company: CompanyDetail, context: Context) {
    val location = getLocation(context, company)
    location?.let {
        val cameraPositionState = rememberCameraPositionState {
            position = CameraPosition.fromLatLngZoom(location, 15f)
        }
        var uiSettings by remember {
            mutableStateOf(
                MapUiSettings(
                    myLocationButtonEnabled = true,
                    rotationGesturesEnabled = true,
                    scrollGesturesEnabled = true,
                    compassEnabled = true
                )
            )
        }
        val mapStyleOptions = if (isSystemInDarkTheme()){
            MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style_dark)
        } else {
            MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style_retro)
        }
        val properties by remember {
            mutableStateOf(
                MapProperties(
                mapType = MapType.NORMAL,
                mapStyleOptions = mapStyleOptions
                )
            )
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .height(250.dp),
        ) {

            GoogleMap(
                properties = properties,
                uiSettings = uiSettings,
                cameraPositionState = cameraPositionState
            ) {
                Marker(
                    position = location,
                    title = company.name
//                    snippet = "Additional explanation"
                )
            }
            Switch(
                checked = uiSettings.zoomControlsEnabled,
                onCheckedChange = {
                    uiSettings = uiSettings.copy(zoomControlsEnabled = it)
                }
            )
        }
    }
}

@Composable
private fun CommentsContainer(comments: List<Comment>){
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp, top = 20.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
        ){
            Text(
                text = "Comments:",
                style = MaterialTheme.typography.subtitle1,
            )
            for (comment in comments){
                    CommentsCard(comment)
            }
            CommentTextField()
        }
    }
}

@Composable
private fun CommentsCard(comment: Comment) {

    var isExpanded by remember { mutableStateOf(false) }
        // surfaceColor will be updated gradually from one color to the other
    val surfaceColor: Color by animateColorAsState(
            if (isExpanded) MaterialTheme.colors.secondary else MaterialTheme.colors.surface,
        )

    Surface(
        shape = MaterialTheme.shapes.medium,
        elevation = 8.dp,
        // surfaceColor color will be changing gradually from primary to surface
        color = surfaceColor,
        // animateContentSize will change the Surface size gradually
        modifier = Modifier
            .animateContentSize()
            .padding(4.dp)
    ) {
        SelectionContainer {
            Column(
                modifier = Modifier
                    .padding(start = 4.dp)
                    .fillMaxWidth()
                    .clickable { isExpanded = !isExpanded }
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    comment.userEmail?.let {
                        Text(
                            text = it,
                            style = MaterialTheme.typography.subtitle2,
                            modifier = Modifier
                                .padding(end = 8.dp)
                        )
                    }
                    comment.date?.let {
                        Text(
                            text = TimeStampToDateTimeConverter.timestampToDateTime("dd/MM/yyyy HH:mm:ss", it),
                            style = MaterialTheme.typography.body1
                        )
                    }
                }
                comment.comment?.let {
                    Text(
                        text = it,
                        style = MaterialTheme.typography.body1,
                        color = MaterialTheme.colors.primaryVariant,
                        maxLines = if (isExpanded) Int.MAX_VALUE else 1,
                    )
                }
            }
        }

    }
    Spacer(modifier = Modifier.height(8.dp))
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CommentTextField() {
    var commentText by rememberSaveable { mutableStateOf("") }
    val kc = LocalSoftwareKeyboardController.current

    TextField(
            value = commentText,
            shape = RoundedCornerShape(8.dp),
            onValueChange = { commentText = it },
            label = { Text("Enter comment: ") },
            modifier = Modifier
                .fillMaxWidth(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text, imeAction = ImeAction.Default),
            keyboardActions = KeyboardActions(onDone = {kc?.hide()}),
            trailingIcon = {
                IconButton(
                    onClick = {
                        postComment(commentText)
                        commentText = ""
                        kc?.hide()
                    }
                ) {
                    Icon(imageVector = Icons.Filled.Done, "post")
                }
            }
        )
}

private fun postComment(commentText: String) {
    if (!commentText.isNullOrBlank()){
        val trimmedCommentText = commentText.trim()
        val timestamp = Timestamp.now()
        val comment = Comment(firebaseUser?.email, company?.orbNumber, timestamp, trimmedCommentText)
        viewModel?.postComment(comment)
    }
}

@Composable
fun CircularIndeterminateProgressBar(isDisplayed: Boolean) {
    if (isDisplayed) {
        Row(
            modifier = Modifier
                .fillMaxSize(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            CircularProgressIndicator(
                color = Teal200
            )
        }
    }
}