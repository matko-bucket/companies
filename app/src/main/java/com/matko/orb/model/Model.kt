package com.matko.orb.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.Timestamp
import com.google.gson.annotations.SerializedName

data class CompanySimple(
    @SerializedName("orb_num")
    val orbNumber: String?,
    val name: String?,
    val city: String?,
    @SerializedName("company_status")
    val active: String?,
    @SerializedName("fetch_url")
    val fetchUrl: String?

)

@Entity
data class CompanyDetail(
    @ColumnInfo(name = "orb_number")
    @SerializedName("orb_num")
    val orbNumber: String?,
    val name: String?,
    val website: String?,
    val phone: String?,
    val email: String?,
    val employees: Int?,
    val description: String?,
    val country: String?,
    val city: String?,
    val address: String?,
    val latitude: Double?,
    val longitude: Double?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    constructor() : this(null,null,null,null,null,null,
        null,null,null,null,null,null)
}

data class CompanySimpleWrapper(
    @SerializedName("results")
    val results: List<CompanySimple>
)

data class Industries(
    val name: String?
) {
    override fun toString(): String {
        return name.toString()
    }
}

data class Address(
    val address1: String?,
    val city: String?,
    val country: String?
)

data class CompanyDetailWrapper(
    @SerializedName("orb_num")
    val orbNumber: String?,
    val name: String?,
    val website: String?,
    val phone: String?,
    val email: String?,
    val employees: Int?,
    val description: String?,
    @SerializedName("address")
    val address: Address,
    val latitude: Double?,
    val longitude: Double?
)

data class SearchFilters(
    val industry: String,
    val country: String
)

data class Comment(
    val userEmail: String?,
    val orbNumber: String?,
    val date: Timestamp?,
    val comment: String?
) {
    constructor() : this(null, null, null, null)
}