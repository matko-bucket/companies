package com.matko.orb.model

import androidx.room.*

@Dao
interface CompanyDao {
    @Insert
    suspend fun insert(vararg company: CompanyDetail) : List<Long>

    @Query("SELECT * FROM companydetail")
    suspend fun getAllCompanyDetails(): List<CompanyDetail>

    @Query("SELECT * FROM companydetail WHERE orb_number = :orbNumber")
    suspend fun getCompanyDetail(orbNumber: String): CompanyDetail?

    @Query("DELETE FROM companydetail")
    suspend fun deleteAllCompanies()

    @Query("DELETE FROM companydetail WHERE orb_number = :orbNumber")
    suspend fun deleteByOrbNumber(orbNumber: String) : Int

    @Delete
    suspend fun deleteCompanyFromDB(company: CompanyDetail)

    @Query("SELECT * FROM companydetail WHERE name LIKE :name")
    suspend fun searchByCompanyName(name: String) : List<CompanyDetail>

}