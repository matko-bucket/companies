package com.matko.orb.model

import android.content.Context
import android.database.sqlite.SQLiteException
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [CompanyDetail::class], version = 3, exportSchema = false)
abstract class CompanyDatabase : RoomDatabase(){
    abstract fun CompanyDao() : CompanyDao

    companion object {
        @Volatile private var instance: CompanyDatabase? = null
        private val LOCK = Any()

        private val MIGRATION = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                try {
                    database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'country' TEXT")
                } catch (e: SQLiteException){
                    // column already exists
                }
                try {
                    database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'city' TEXT")
                } catch (e: SQLiteException){
                    // column already exists
                }
                try {
                    database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'address' TEXT")
                } catch (e: SQLiteException){
                    // column already exists
                }
                try {
                    database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'latitude' DOUBLE")
                } catch (e: SQLiteException){
                    // column already exists
                }
                try {
                    database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'longitude' DOUBLE")
                } catch (e: SQLiteException){
                    // column already exists
                    Log.d("baza", e.message.toString())
                }
            }
        }
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }
        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            CompanyDatabase::class.java,
            "companies"
        )
            .addMigrations(MIGRATION)
            .build()
    }

}