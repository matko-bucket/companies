package com.matko.orb.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.matko.orb.model.CompanyDetail

class FavoritesFirebaseDbViewModel : ViewModel(){

    private val db = Firebase.firestore
    val companies = MutableLiveData<List<CompanyDetail>>()
    private lateinit var listenerRegistration: ListenerRegistration

    fun fetchAllFromFirebaseDb(userEmail: String) {
        listenerRegistration = db.collection("users/$userEmail/favorite_companies")
            .addSnapshotListener { value, e ->
                if (e != null)
                    return@addSnapshotListener
                val favorites = ArrayList<CompanyDetail>()
                for (doc in value!!){
                    val company = doc.toObject<CompanyDetail>()
                    favorites.add(company)
                }
                companies.value = favorites
            }
    }

    fun searchByName(userEmail: String, companyName: String) {
        if (companyName.isNotEmpty()){
            db.collection("users/$userEmail/favorite_companies")
//                .whereEqualTo("name", companyName)
                .orderBy("name")
                .startAt(companyName)
                .endAt(companyName + "\uf8ff")
//                .whereGreaterThanOrEqualTo("name", companyName)
//                .whereLessThan("name", companyName +"z")
                .get()
                .addOnSuccessListener { results ->
                    val favorites = ArrayList<CompanyDetail>()
                    for (doc in results){
                        val company = doc.toObject<CompanyDetail>()
                        favorites.add(company)
                    }
                    companies.value = favorites
                }
        } else {
            fetchAllFromFirebaseDb(userEmail)
        }
    }

    fun detachListener(){
        if (this::listenerRegistration.isInitialized)
            listenerRegistration.remove()
    }

}