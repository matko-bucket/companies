package com.matko.orb.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.matko.orb.api.CompanyApiService
import com.matko.orb.model.Comment
import com.matko.orb.model.CompanyDatabase
import com.matko.orb.model.CompanyDetail
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DetailsViewModel() : ViewModel() {

    val currentUser = MutableLiveData<FirebaseUser>()
    private val companyApiService = CompanyApiService()
    val attributes = arrayListOf("name", "country", "city", "address",
        "website", "phone", "email", "employees", "description")

//    For this way to work, we need application/ inherit from AndroidVewModel
//    val companyAttributesTitle =     arrayListOf(
//        getString(R.string.company_name_search_hint),
//        getString(R.string.company_country_search_hint),
//        getString(R.string.company_city),
//        getString(R.string.address),
//        getString(R.string.website),
//        getString(R.string.phone),
//        getString(R.string.email),
//        getString(R.string.number_of_employees),
//        getString(R.string.description)
//    )

    val company = MutableLiveData<CompanyDetail>()
    val loadError = MutableLiveData(false)
    val loading = MutableLiveData(false)
    val isCompanyExistingInDatabases = MutableLiveData<Boolean>()
    private val firebaseDB = Firebase.firestore
    val navigateToMapFragment = MutableLiveData(false)
    val comments = MutableLiveData<List<Comment>>()
    val savedToRoomDb = MutableLiveData<Boolean>()
    val savedToFirebaseDb = MutableLiveData<Boolean>()
    val deletedFromRoomDb = MutableLiveData<Boolean>()
    val deletedFromFirebase = MutableLiveData<Boolean>()
    private lateinit var listenerRegistration: ListenerRegistration

    fun fetchFromRemote(fetchUrl: String) {
        loading.value = true
        loadError.value = false
        viewModelScope.launch {
            kotlin.runCatching { companyApiService.fetchCompanyDetailsV2(fetchUrl) }
                .onSuccess {
                    company.value = CompanyDetail(
                        it.orbNumber,
                        it.name,
                        it.website,
                        it.phone,
                        it.email,
                        it.employees,
                        it.description,
                        it.address.country,
                        it.address.city,
                        it.address.address1,
                        it.latitude,
                        it.longitude
                    )
                    loadError.value = false
                    loading.value = false
                }
                .onFailure {
                    loadError.value = true
                    loading.value = false
                }
        }
    }

    fun saveCompanyLocally(context: Context, company: CompanyDetail) {
        viewModelScope.launch {
            val insertedList = CompanyDatabase(context).CompanyDao().insert(company)
            savedToRoomDb.value = !insertedList.isNullOrEmpty()
            isCompanyExistingInDatabases.value = true
        }
    }

    fun deleteCompanyFromRoomDB(context: Context, orbNumber: String) {
        viewModelScope.launch {
            val affectedRow = CompanyDatabase(context).CompanyDao().deleteByOrbNumber(orbNumber)
            deletedFromRoomDb.value = affectedRow == 1
            isCompanyExistingInDatabases.value = affectedRow != 1
        }
    }

    fun getCompanyFromRoomDB(context: Context, orbNum: String?) {
        loading.value = true
        viewModelScope.launch {
            delay(1000)
            val companyFromDB = orbNum?.let {
                CompanyDatabase(context).CompanyDao().getCompanyDetail(it)
            }
            if (companyFromDB != null) {
                company.value = companyFromDB!!
                isCompanyExistingInDatabases.value = true
            } else {
                loadError.value =true
            }
            loading.value = false
        }
    }

    fun isCompanyExistingInDatabases(context: Context, orbNum: String?, userEmail: String?) {
        viewModelScope.launch {
            val companyFromDB = orbNum?.let {
                CompanyDatabase(context).CompanyDao().getCompanyDetail(it)
            }
            if (companyFromDB != null) {
                isCompanyExistingInDatabases.value = true
            } else {
                firebaseDB.collection("users/$userEmail/favorite_companies").document("$orbNum")
                    .get()
                    .addOnSuccessListener { document ->
                        isCompanyExistingInDatabases.value = document.exists()
                    }
            }
        }
    }

    fun saveToFirebaseDB(companyDetail: CompanyDetail, currentUser: FirebaseUser?) {
        companyDetail.orbNumber?.let { orb ->
            currentUser?.let { user ->
                user.email?.let { it1 ->
                    firebaseDB.collection("users").document(it1).collection("favorite_companies").document(orb)
                        .set(companyDetail)
                        .addOnSuccessListener {
                            savedToFirebaseDb.value = true
                            isCompanyExistingInDatabases.value = true
                        }
                        .addOnFailureListener {
                            savedToFirebaseDb.value = false
                        }
                }
            }
        }
    }

    fun getCompanyFromFirebaseDB(userEmail: String, orbNum: String?) {
        loading.value = true
        viewModelScope.launch {
            delay(1000)
            firebaseDB.collection("users/$userEmail/favorite_companies").document("$orbNum")
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        company.value = document.toObject<CompanyDetail>()
                    }
                    isCompanyExistingInDatabases.value = document != null
                    loading.value = false
                    loadError.value = false
                    getCommentsFromFirebaseDB(orbNum)
                }
                .addOnFailureListener {
                    loadError.value = true
                    loading.value = false
                }
        }
    }

    fun deleteFromFirebaseDB(userEmail: String, orbNum: String?) {
        firebaseDB.collection("users/$userEmail/favorite_companies").document("$orbNum")
            .delete()
            .addOnSuccessListener {
                deletedFromFirebase.value = true
                isCompanyExistingInDatabases.value = false
            }
            .addOnFailureListener {
                deletedFromFirebase.value = false
            }
    }

    private fun getCommentsFromFirebaseDB(orbNum: String?) {
        listenerRegistration = firebaseDB.collection("comments2")
            .whereEqualTo("orbNumber", orbNum)
            .addSnapshotListener{ value, e ->
                if (e != null)
                    return@addSnapshotListener
                val commentsList = ArrayList<Comment>()
                for (doc in value!!){
                    val comment = doc.toObject<Comment>()
                    commentsList.add(comment)
                }
                val sortedList = commentsList.sortedWith(compareBy { it.date })

                comments.value = sortedList
            }
    }

    fun postComment(comment: Comment) {
        firebaseDB.collection("comments2").document()
            .set(comment)
    }

//    private fun getString(int: Int): String {
//        return getApplication<Application>().resources.getString(int)
//    }

    fun detachListener(){
        if (this::listenerRegistration.isInitialized)
            listenerRegistration.remove()
    }
}