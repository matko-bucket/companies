package com.matko.orb.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.matko.orb.model.Comment
import com.matko.orb.model.CompanyDetail
import com.matko.orb.repository.CompanyRepositoryImpl
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DetailsViewModelV2(private val companyRepositoryImpl: CompanyRepositoryImpl):  ViewModel(){

    val currentUser = MutableLiveData<FirebaseUser>()
//    private val companyApiService = CompanyApiService()

//    For this way to work, we need application/ inherit from AndroidVewModel
//    val companyAttributesTitle =     arrayListOf(
//        getString(R.string.company_name_search_hint),
//        getString(R.string.company_country_search_hint),
//        getString(R.string.company_city),
//        getString(R.string.address),
//        getString(R.string.website),
//        getString(R.string.phone),
//        getString(R.string.email),
//        getString(R.string.number_of_employees),
//        getString(R.string.description)
//    )

    val navigateToMapFragment = MutableLiveData(false)
    val savedToRoomDb = MutableLiveData<Boolean>()
    val deletedFromRoomDb = MutableLiveData<Boolean>()

    val company = companyRepositoryImpl.companyLoadSuccess
    val loadError = companyRepositoryImpl.companyLoadError
    val loading = companyRepositoryImpl.loading
    val isCompanyExistingInDatabases = companyRepositoryImpl.isCompanyExistingInDatabases
    val comments = companyRepositoryImpl.commentsLoadSuccess
    val savedToFirebaseDb = companyRepositoryImpl.savedToFirebaseDb
    val deletedFromFirebase = companyRepositoryImpl.deletedFromFirebaseDb



    fun fetchFromRemote(fetchUrl: String) {
        loading.value = true
        loadError.value = false
        viewModelScope.launch {
            kotlin.runCatching { companyRepositoryImpl.fetchFromRemote(fetchUrl) }
                .onSuccess {
                    company.value = CompanyDetail(
                        it.orbNumber,
                        it.name,
                        it.website,
                        it.phone,
                        it.email,
                        it.employees,
                        it.description,
                        it.address.country,
                        it.address.city,
                        it.address.address1,
                        it.latitude,
                        it.longitude
                    )
                    loadError.value = false
                    loading.value = false
                }
                .onFailure {
                    loadError.value = true
                    loading.value = false
                }
        }
    }

    fun saveCompanyLocally(context: Context, company: CompanyDetail) {
        viewModelScope.launch {
            val insertedList = companyRepositoryImpl.saveCompanyInRoomDb(company)
            savedToRoomDb.value = !insertedList.isNullOrEmpty()
            isCompanyExistingInDatabases.value = true
        }
    }

    fun deleteCompanyFromRoomDB(context: Context, orbNumber: String) {
        viewModelScope.launch {
            val affectedRow = companyRepositoryImpl.deleteCompanyFromRoomDB(orbNumber)
            deletedFromRoomDb.value = affectedRow == 1
            isCompanyExistingInDatabases.value = affectedRow != 1
        }
    }

    fun getCompanyFromRoomDB(context: Context, orbNum: String?) {
        loading.value = true
        viewModelScope.launch {
            delay(1000)
            val companyFromDB = orbNum?.let {
                companyRepositoryImpl.getCompanyFromRoomDb(orbNum)
            }
            if (companyFromDB != null) {
                company.value = companyFromDB!!
                isCompanyExistingInDatabases.value = true
            } else {
                loadError.value =true
            }
            loading.value = false
        }
    }

    fun isCompanyExistingInDatabases(context: Context, orbNum: String?, userEmail: String?) {
        viewModelScope.launch {
            val companyFromDB = orbNum?.let {
                companyRepositoryImpl.getCompanyFromRoomDb(it)
            }
            if (companyFromDB != null) {
                isCompanyExistingInDatabases.value = true
            } else {
                companyRepositoryImpl.getCompanyFromFirebaseDB(userEmail, orbNum)
            }
        }
    }

    fun saveToFirebaseDB(companyDetail: CompanyDetail, currentUser: FirebaseUser?) {
        viewModelScope.launch {
            companyRepositoryImpl.saveToFirebaseDB(companyDetail, currentUser)
        }
    }

    fun getCompanyFromFirebaseDB(userEmail: String, orbNum: String?) {
        loading.value = true
        viewModelScope.launch {
            delay(1000)
            companyRepositoryImpl.getCompanyFromFirebaseDB(userEmail, orbNum)
        }
    }

    fun deleteFromFirebaseDB(userEmail: String, orbNum: String?) {
        viewModelScope.launch {
            companyRepositoryImpl.deleteFromFirebaseDB(userEmail,orbNum)
        }
    }

    fun postComment(comment: Comment) {
        companyRepositoryImpl.postComment(comment)
    }

//    private fun getString(int: Int): String {
//        return getApplication<Application>().resources.getString(int)
//    }

    fun detachListener(){
        companyRepositoryImpl.detachListener()
    }

    companion object {
        private var instance: DetailsViewModelV2? = null

        fun getInstance(companyRepositoryImpl: CompanyRepositoryImpl): DetailsViewModelV2{
//            if (instance != null)
//                return instance as DetailsViewModelV2
//            else {
//                instance = DetailsViewModelV2(companyRepositoryImpl)
//                return instance as DetailsViewModelV2
//            }
            instance = DetailsViewModelV2(companyRepositoryImpl)
            return instance as DetailsViewModelV2
        }
    }

}