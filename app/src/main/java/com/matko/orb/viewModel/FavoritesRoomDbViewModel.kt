package com.matko.orb.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.matko.orb.model.CompanyDatabase
import com.matko.orb.model.CompanyDetail
import kotlinx.coroutines.launch

class FavoritesRoomDbViewModel : ViewModel() {

    val companies = MutableLiveData<List<CompanyDetail>>()


    fun fetchFromRoomDB(context: Context) {
        viewModelScope.launch {
            companies.value = CompanyDatabase(context).CompanyDao().getAllCompanyDetails()
        }
    }

    fun searchByName(context: Context, companyName: String) {
        viewModelScope.launch {
            companies.value = CompanyDatabase(context).CompanyDao().searchByCompanyName(companyName)
        }
    }

}