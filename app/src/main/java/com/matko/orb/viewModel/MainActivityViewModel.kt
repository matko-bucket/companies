package com.matko.orb.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.matko.orb.api.CompanyApiService
import com.matko.orb.model.Industries
import com.matko.orb.model.SearchFilters
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {

    val searchFilters = MutableLiveData<SearchFilters>()
    private val companyApiService = CompanyApiService()
    val industriesList = MutableLiveData<List<Industries>>()
    val isLoggedIn = MutableLiveData<Boolean>()
    val currentUser = MutableLiveData<FirebaseUser>()


    fun fetchIndustries() {
        viewModelScope.launch {
            kotlin.runCatching { companyApiService.fetchIndustries() }
                .onSuccess {
                    industriesList.value = it
                }
                .onFailure {
                    industriesList.value = arrayListOf()
                }
        }
    }

}