package com.matko.orb.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.*

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

object Constants{
    const val BASE_URL = "https://api.orb-intelligence.com/3/"
    const val API_KEY = "c66c5dad-395c-4ec6-afdf-7b78eb94166a"
    const val SELECT_INDUSTRY = "Select industry"
    const val SHARED_PREF_FILE = "com_matko_orb_filters"
    const val FILTER_COUNTRY = "com_matko_orb_country"
    const val FILTER_INDUSTRY = "com_matko_orb_industry"
    const val IS_LOGGED_IN = "com_matko_orb_is_logged_in"
    const val TAG_GOOGLE_SIGN_IN = "GoogleActivity"
    const val RC_SIGN_IN = 9001

}

class EmailValidator {
    companion object {
        private const val EMAIL_REGEX = "^[\\w\\.-]+([\\+]?[\\d]+)?@([\\w\\-]+\\.)+[A-Za-z]{2,20}$"
        private const val PASSWORD_REGEX = "(?=.*[0-9a-zA-Z]).{6,}"

        fun isEmailValid( email: String): Boolean {
            return EMAIL_REGEX.toRegex().matches(email)
        }

        fun isPasswordValid( password: String): Boolean {
            return PASSWORD_REGEX.toRegex().matches(password)
        }
    }
}

class TimeStampToDateTimeConverter {
    companion object {
         fun timestampToDateTime(pattern: String, timeStamp: Timestamp): String {
             val dateFormatter = SimpleDateFormat(pattern, Locale.GERMAN)
             return dateFormatter.format(timeStamp.toDate())
         }
    }
}

