package com.matko.orb.util

import android.content.Context
import android.view.MotionEvent
import com.google.android.gms.maps.MapView

class CustomMapView(context: Context): MapView(context) {
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_UP -> parent.requestDisallowInterceptTouchEvent(false)
            MotionEvent.ACTION_DOWN -> parent.requestDisallowInterceptTouchEvent(true)
        }
        return super.dispatchTouchEvent(ev)
    }
}