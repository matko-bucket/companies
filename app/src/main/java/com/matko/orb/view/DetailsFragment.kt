package com.matko.orb.view

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.view.*
import android.widget.ScrollView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.firebase.auth.FirebaseUser
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.matko.orb.R
import com.matko.orb.api.CompanyApiService
import com.matko.orb.databinding.FragmentDetailsBinding
import com.matko.orb.model.CompanyDatabase
import com.matko.orb.model.CompanyDetail
import com.matko.orb.repository.CompanyRepositoryImpl
import com.matko.orb.viewModel.DetailsViewModel
import com.matko.orb.viewModel.DetailsViewModelV2
import com.matko.orb.viewModel.MainActivityViewModel
import java.lang.Exception

class DetailsFragment : Fragment(), OnMapReadyCallback {

//    private val detailsViewModel: DetailsViewModel by viewModels()
    private lateinit var binding: FragmentDetailsBinding
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private var company: CompanyDetail? = null
    private var loadedFromRoomDB = false
    private var loadedFromFirebaseDB = false
    private var currentUser: FirebaseUser? = null
    private lateinit var mScrollView: ScrollView
    private lateinit var mMapView: MapView
    private lateinit var detailsViewModel: DetailsViewModelV2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val db: CompanyDatabase = CompanyDatabase.invoke(requireContext())
        val api: CompanyApiService = CompanyApiService()
        val repo: CompanyRepositoryImpl = CompanyRepositoryImpl(api, db)
        detailsViewModel = DetailsViewModelV2.getInstance(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var fetchUrl = ""
        var orbFromDB = ""
        var isFromFirebase = false

        mMapView = binding.map
        mMapView.isVisible = true
        mMapView.let {
            mMapView.removeAllViews()
        }
        mMapView.getMapAsync(this)
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume()
        mScrollView = binding.scrollView
        arguments?.let {
            fetchUrl = DetailsFragmentArgs.fromBundle(it).fetchUrl
            orbFromDB = DetailsFragmentArgs.fromBundle(it).orbNumber
            isFromFirebase = DetailsFragmentArgs.fromBundle(it).isForFirebase
        }

        if (isFromFirebase) {
            loadedFromFirebaseDB = true
            loadedFromRoomDB = false
            mainActivityViewModel.currentUser.observe(viewLifecycleOwner) { currentUser ->
                currentUser?.let {
                    this.currentUser = it
                    it.email?.let { it1 ->
                        detailsViewModel.getCompanyFromFirebaseDB(it1, orbFromDB)
                    }
                }
            }
        } else if (orbFromDB != "0") {
            loadedFromFirebaseDB = false
            loadedFromRoomDB = true
            detailsViewModel.getCompanyFromRoomDB(requireContext(), orbFromDB)
        } else if (!fetchUrl.isNullOrEmpty()) {
            loadedFromFirebaseDB = false
            loadedFromRoomDB = false
            detailsViewModel.fetchFromRemote(fetchUrl)
        }

        binding.refreshLayoutDetails.setOnRefreshListener {
            if (loadedFromFirebaseDB) {
                currentUser?.email?.let { detailsViewModel.getCompanyFromFirebaseDB(it, orbFromDB) }
            } else if (loadedFromRoomDB) {
                detailsViewModel.getCompanyFromRoomDB(requireContext(), orbFromDB)
            } else if (!fetchUrl.isNullOrEmpty()) {
                detailsViewModel.fetchFromRemote(fetchUrl)
            }
            binding.errorMessage.isVisible = false
            binding.refreshLayoutDetails.isRefreshing = false
        }

        binding.addressLayout.setOnClickListener { addressLayout ->
            company?.let {
                var address = it.address
                val action = DetailsFragmentDirections.actionDetailsFragmentToMapFragment()
                address?.let { addr ->
                    if (!it.city.isNullOrEmpty()) {
                        address = address + ", " + it.city
                    }
                    action.address = addr

                    it.name?.let { name ->
                        action.companyName = name
                    }
                    it.latitude?.let { latitude ->
                        action.latitude = latitude.toFloat()
                    }
                    it.longitude?.let { longitude ->
                        action.longitude = longitude.toFloat()
                    }
                }
//                Navigation.findNavController(addressLayout).navigate(action)
                findNavController().navigate(action)
            }
//                    val gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
//                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//                    mapIntent.setPackage("com.google.android.apps.maps")
//                    startActivity(mapIntent)
        }
        observeViewModel()

    }

    private fun observeViewModel() {
        mainActivityViewModel.currentUser.observe(viewLifecycleOwner) { currentUser ->
            currentUser?.let {
                this.currentUser = it
            }
        }

        detailsViewModel.company.observe(viewLifecycleOwner) { company ->
            company?.let {
                with(binding) {
                    errorMessage.visibility = View.GONE
                    companyName.text = it.name
                    country.text = it.country
                    city.text = it.city
                    address.text = it.address
                    website.text = it.website
                    phone.text = it.phone
                    email.text = it.email
                    employees.text = it.employees.toString()
                    description.text = it.description
                }
                this.company = it

                if (!loadedFromFirebaseDB && !loadedFromRoomDB) {
                    detailsViewModel.isCompanyExistingInDatabases(
                        requireContext(),
                        it.orbNumber,
                        currentUser?.email
                    )
                }
            }
        }

        detailsViewModel.loadError.observe(viewLifecycleOwner) {
                binding.errorMessage.isVisible = it
                binding.dataLayout.isVisible = !it
        }

        detailsViewModel.loading.observe(viewLifecycleOwner) {
                binding.loadingViewDetails.isVisible = it
                if (it) binding.errorMessage.isVisible = false
        }

        detailsViewModel.savedToRoomDb.observe(viewLifecycleOwner) {
            if (it){
                Toast.makeText(context, R.string.saved_in_room_db, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_saved_in_room_db, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.savedToFirebaseDb.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.saved_in_firestore, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_saved_in_firestore, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.deletedFromRoomDb.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.deleted_from_room_db, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_deleted_from_room_db, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.deletedFromFirebase.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.deleted_from_firestore, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_deleted_from_firebase_db, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorites_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val iconSave = menu.findItem(R.id.action_save)
        val iconDelete = menu.findItem(R.id.action_delete)
        detailsViewModel.isCompanyExistingInDatabases.observe(viewLifecycleOwner) { isExisting ->
            iconSave.isVisible = !isExisting
            iconDelete.isVisible = isExisting
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                company?.let { it ->
                    detailsViewModel.saveCompanyLocally(requireContext(), it)
                    detailsViewModel.saveToFirebaseDB(it, currentUser)
                }
                true
            }
            R.id.action_delete -> {
                company?.let { it ->
                    it.orbNumber?.let { orb ->
                        detailsViewModel.deleteCompanyFromRoomDB(requireContext(), orb)
                        currentUser?.email?.let { userEmail ->
                            detailsViewModel.deleteFromFirebaseDB(userEmail, orb)
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.clear()
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.setOnCameraMoveListener {
            mMapView.parent.requestDisallowInterceptTouchEvent(true)
        }
        googleMap.setOnCameraIdleListener {
            mMapView.parent.requestDisallowInterceptTouchEvent(false)
        }
        detailsViewModel.company.observe(viewLifecycleOwner) {
            it?.let {
                prepareGoogleMap(it, googleMap)
            }
        }
    }

    private fun getLatLngFromAddress(context: Context, address: String): LatLng? {
        val geoCoder = Geocoder(context)
        val p1: LatLng? = try {
            val addresses = geoCoder.getFromLocationName(address, 5)
            val location = addresses[0]
            LatLng(location.latitude, location.longitude)
        } catch (e: Exception) {
            null
        }
        return p1
    }

    private fun prepareGoogleMap(it: CompanyDetail, googleMap: GoogleMap) {
        val companyLocation = if (it.latitude != null && it.longitude != null) {
            LatLng(it.latitude, it.longitude)
        } else {
            it.address?.let { it1 -> getLatLngFromAddress(requireContext(), it1) }
        }

        if (companyLocation != null) {
            val cameraPosition = CameraPosition.builder()
                    .target(companyLocation)
                    .zoom(15f)
                    .bearing(0f)
                    .tilt(10f)
                    .build()
            val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            googleMap.animateCamera(cameraUpdate)
            val markerOptions = MarkerOptions().position(companyLocation).title(it.name)
            googleMap.addMarker(markerOptions)
        } else {
            Toast.makeText(requireContext(), R.string.location_not_found, Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        detailsViewModel.company.value = null
        super.onDestroyView()
    }
}