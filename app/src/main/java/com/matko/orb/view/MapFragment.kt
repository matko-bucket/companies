package com.matko.orb.view

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.matko.orb.R
import com.matko.orb.databinding.FragmentMapBinding
import java.lang.Exception


class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMapView: MapView
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var binding: FragmentMapBinding
    private var address = ""
    private var companyName = ""
    private var companyLatitude: Float? = null
    private var companyLongitude: Float? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMapBinding.inflate(inflater, container, false)
        mMapView = binding.map
        mMapView.getMapAsync(this)
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            address = MapFragmentArgs.fromBundle(it).address
            companyName = MapFragmentArgs.fromBundle(it).companyName
            companyLatitude = MapFragmentArgs.fromBundle(it).latitude
            companyLongitude = MapFragmentArgs.fromBundle(it).longitude
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.clear()
        mGoogleMap = googleMap
        mGoogleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        val companyLocation: LatLng? = if (companyLatitude != null && companyLongitude != null && companyLatitude != 0f && companyLongitude != 0f) {
            LatLng(companyLatitude!!.toDouble(), companyLongitude!!.toDouble())
        } else {
            getLatLngFromAddress(requireContext(), address)
        }

        if (companyLocation != null) {
            val cameraPosition =
                CameraPosition.builder()
                    .target(companyLocation)
                    .zoom(15f)
                    .bearing(0f)
                    .tilt(10f)
                    .build()
            val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mGoogleMap.animateCamera(cameraUpdate)
            val markerOptions = MarkerOptions().position(companyLocation).title(companyName)
            mGoogleMap?.addMarker(markerOptions)
        } else {
            Toast.makeText(requireContext(), R.string.location_not_found, Toast.LENGTH_LONG).show()
        }
    }

    private fun getLatLngFromAddress(context: Context, address: String) : LatLng?{
        val geoCoder = Geocoder(context)
        val p1: LatLng? = try {
            val addresses = geoCoder.getFromLocationName(address, 5)
            val location = addresses[0]
            LatLng(location.latitude, location.longitude)
        } catch (e: Exception){
            null
        }
        return p1
    }
}