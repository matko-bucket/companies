package com.matko.orb.view

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.matko.orb.databinding.FragmentFavoritesFirebaseDbBinding
import com.matko.orb.viewModel.FavoritesFirebaseDbViewModel
import com.matko.orb.viewModel.MainActivityViewModel

class FavoritesFirebaseDbFragment : Fragment() {

    private val firebaseDbViewModel: FavoritesFirebaseDbViewModel by viewModels()
    private lateinit var binding: FragmentFavoritesFirebaseDbBinding
    private val favoritesListAdapter = FavoritesRecycleViewAdapter(arrayListOf())
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private var userEmail: String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesFirebaseDbBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivityViewModel.currentUser.observe(viewLifecycleOwner) {user ->
            if (user != null) {
                user.email?.let {
                    userEmail = it
                    firebaseDbViewModel.fetchAllFromFirebaseDb(it)
                }
            } else {
                firebaseDbViewModel.companies.value = null
                favoritesListAdapter.updateCompanyList(ArrayList(), true)
            }
            binding.notLoggedInTitle.isVisible = user == null
        }

        observeViewModel()

        binding.companyFavoritesList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = favoritesListAdapter
        }

        binding.searchByName.doAfterTextChanged { editable: Editable? ->
            userEmail?.let {
                firebaseDbViewModel.searchByName(it, editable.toString())
            }
        }
    }

    private fun observeViewModel() {
        firebaseDbViewModel.companies.observe(viewLifecycleOwner, Observer { companies ->
            companies?.let {
                binding.companyFavoritesList.isVisible = it.isNotEmpty()
                binding.listEmpty.isVisible = it.isEmpty()
                if (it.isNotEmpty()) {
                    favoritesListAdapter.updateCompanyList(it, true)
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        firebaseDbViewModel.detachListener()
    }
}