package com.matko.orb.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.matko.orb.databinding.ItemFavoriteBinding
import com.matko.orb.model.CompanyDetail

class FavoritesRecycleViewAdapter(private val companyList: ArrayList<CompanyDetail>) :
    RecyclerView.Adapter<FavoritesRecycleViewAdapter.FavoriteViewHolder>() {

    private lateinit var binding: ItemFavoriteBinding
    private var isCallFromFirebaseDbFragment: Boolean = false

    fun updateCompanyList(newCompanyList: List<CompanyDetail>, isCallFromFirebaseDbFragment: Boolean) {
        this.isCallFromFirebaseDbFragment = isCallFromFirebaseDbFragment
        companyList.clear()
        companyList.addAll(newCompanyList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemFavoriteBinding.inflate(inflater, parent, false)
        return FavoriteViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val item = holder.itemFavoriteBinding
        item.city.text = companyList[position].city
        item.name.text = companyList[position].name
        item.orbNumber.text = companyList[position].orbNumber

        item.itemLayout.setOnClickListener {
            val action = FavoritesFragmentDirections.actionFavoritesFragmentToDetailsFragmentCompose()
            action.orbNumber = item.orbNumber.text.toString()
            action.isForFirebase = isCallFromFirebaseDbFragment
            Navigation.findNavController(it).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return companyList.size
    }

    class FavoriteViewHolder(var itemFavoriteBinding: ItemFavoriteBinding) :
        RecyclerView.ViewHolder(itemFavoriteBinding.root)
}