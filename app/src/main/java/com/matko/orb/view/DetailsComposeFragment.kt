package com.matko.orb.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseUser
import com.matko.orb.R
import com.matko.orb.api.CompanyApiService
import com.matko.orb.model.CompanyDatabase
import com.matko.orb.model.CompanyDetail
import com.matko.orb.repository.CompanyRepositoryImpl
import com.matko.orb.ui.composable.CompanyDetailsScreen
import com.matko.orb.ui.theme.DetailsFragmentComposeTheme
import com.matko.orb.viewModel.MainActivityViewModel
import com.matko.orb.viewModel.DetailsViewModelV2

class DetailsComposeFragment : Fragment() {

//    private val detailsViewModel: DetailsViewModel by viewModels()
//    private val detailsVewModel: DetailsViewModelV2 by viewModels()
    private lateinit var detailsViewModel: DetailsViewModelV2
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private var company: CompanyDetail? = null
    private var loadedFromRoomDB = false
    private var loadedFromFirebaseDB = false
    private var currentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val db: CompanyDatabase = CompanyDatabase.invoke(requireContext())
        val api: CompanyApiService = CompanyApiService()
        val repo: CompanyRepositoryImpl = CompanyRepositoryImpl(api, db)
        detailsViewModel = DetailsViewModelV2.getInstance(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val context = requireContext()
        return ComposeView(context).apply {
            setContent {
                DetailsFragmentComposeTheme {
                    CompanyDetailsScreen(detailsViewModel, context)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var fetchUrl = ""
        var orbFromDB = ""
        var isFromFirebase = false
        arguments?.let {
            fetchUrl = DetailsFragmentArgs.fromBundle(it).fetchUrl
            orbFromDB = DetailsFragmentArgs.fromBundle(it).orbNumber
            isFromFirebase = DetailsFragmentArgs.fromBundle(it).isForFirebase
        }

        if (isFromFirebase) {
            loadedFromFirebaseDB = true
            loadedFromRoomDB = false
            mainActivityViewModel.currentUser.observe(viewLifecycleOwner) { currentUser ->
                currentUser?.let {
                    this.currentUser = it
                    detailsViewModel.currentUser.value = it
                    it.email?.let { it1 ->
                        detailsViewModel.getCompanyFromFirebaseDB(it1, orbFromDB)
                    }
                }
            }
        } else if (orbFromDB != "0") {
            loadedFromFirebaseDB = false
            loadedFromRoomDB = true
            detailsViewModel.getCompanyFromRoomDB(requireContext(), orbFromDB)
        } else if (!fetchUrl.isNullOrEmpty()) {
            loadedFromFirebaseDB = false
            loadedFromRoomDB = false
            detailsViewModel.fetchFromRemote(fetchUrl)
        }
//        binding.refreshLayoutDetails.setOnRefreshListener {
//            if (loadedFromFirebaseDB) {
//                currentUser?.email?.let { detailsVewModel.getCompanyFromFirebaseDB(it, orbFromDB) }
//            } else if (loadedFromRoomDB) {
//                detailsVewModel.getCompanyFromRoomDB(requireContext(), orbFromDB)
//            } else if (!fetchUrl.isNullOrEmpty()) {
//                detailsVewModel.fetchFromRemote(fetchUrl)
//            }
//            binding.errorMessage.isVisible = false
//            binding.refreshLayoutDetails.isRefreshing = false
//        }
        observeViewModel()
    }

    private fun observeViewModel() {
        mainActivityViewModel.currentUser.observe(viewLifecycleOwner) { currentUser ->
            currentUser?.let {
                this.currentUser = it
            }
        }

        detailsViewModel.company.observe(viewLifecycleOwner) { company ->
            company?.let {
                this.company = it
                if (!loadedFromFirebaseDB && !loadedFromRoomDB) {
                    detailsViewModel.isCompanyExistingInDatabases(requireContext(), it.orbNumber, currentUser?.email)
                }
            }
        }

        detailsViewModel.loadError.observe(viewLifecycleOwner) { isError ->
            isError?.let {
//                binding.errorMessage.isVisible = it
//                binding.dataLayout.isVisible = !it
            }
        }

        detailsViewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            isLoading?.let {
//                binding.loadingViewDetails.isVisible = it
//                if (it) binding.errorMessage.isVisible = false
            }
        }

        detailsViewModel.navigateToMapFragment.observe(viewLifecycleOwner) { toMapFragment ->
            if (toMapFragment){
                company?.let {
                    var address = it.address
                    val action = DetailsComposeFragmentDirections.actionDetailsFragmentComposeToMapFragment()
                    address?.let { addr ->
                        if (!it.city.isNullOrEmpty()) {
                            address = address + ", " + it.city
                        }
                        action.address = addr

                        it.name?.let { name ->
                            action.companyName = name
                        }
                        it.latitude?.let { latitude ->
                            action.latitude = latitude.toFloat()
                        }
                        it.longitude?.let { longitude ->
                            action.longitude = longitude.toFloat()
                        }
                    }
                    findNavController().navigate(action)
                }
                detailsViewModel.navigateToMapFragment.value = false
            }
        }

        detailsViewModel.savedToRoomDb.observe(viewLifecycleOwner) {
            if (it){
                Toast.makeText(context, R.string.saved_in_room_db, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_saved_in_room_db, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.savedToFirebaseDb.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.saved_in_firestore, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_saved_in_firestore, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.deletedFromRoomDb.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.deleted_from_room_db, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_deleted_from_room_db, Toast.LENGTH_SHORT).show()
            }
        }

        detailsViewModel.deletedFromFirebase.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.deleted_from_firestore, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.not_deleted_from_firebase_db, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorites_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val iconSave = menu.findItem(R.id.action_save)
        val iconDelete = menu.findItem(R.id.action_delete)
        detailsViewModel.isCompanyExistingInDatabases.observe(viewLifecycleOwner) { isExisting ->
            iconSave.isVisible = !isExisting
            iconDelete.isVisible = isExisting
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                company?.let { it ->
                    detailsViewModel.saveCompanyLocally(requireContext(), it)
                    detailsViewModel.saveToFirebaseDB(it, currentUser)
                }
                true
            }
            R.id.action_delete -> {
                company?.let { it ->
                    it.orbNumber?.let { orb ->
                        detailsViewModel.deleteCompanyFromRoomDB(requireContext(), orb)
                        currentUser?.email?.let { userEmail ->
                            detailsViewModel.deleteFromFirebaseDB(userEmail, orb)
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        detailsViewModel.company.value = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        detailsViewModel.detachListener()
    }
}