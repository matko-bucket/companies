package com.matko.orb.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.matko.orb.R
import com.matko.orb.databinding.ActivitySignUpBinding
import com.matko.orb.util.EmailValidator

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        binding.signUpButton.setOnClickListener {
            val email = binding.promptEmail.text.toString()
            val password = binding.promptPassword.text.toString()
            createAccount(email, password)

//            if (EmailValidator.isEmailValid(email)) {
//                if (password.length >= 6) {
//                    createAccount(email, password)
//                } else {
//                    binding.promptPassword.error = resources.getString(R.string.invalid_password)
//                }
//            } else {
//                binding.promptEmail.error = R.string.invalid_email.toString()
//            }
        }
    }

    private fun createAccount(email: String, password: String) {
        binding.loader.isVisible = true
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        binding.loader.isVisible = false
        if (user != null) {
            Toast.makeText(baseContext, resources.getString(R.string.welcome) + user.email , Toast.LENGTH_LONG).show()
            finish()
        } else {
            Toast.makeText(baseContext, R.string.auth_failed, Toast.LENGTH_SHORT).show()
        }
    }

}