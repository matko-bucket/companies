package com.matko.orb.view

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import com.matko.orb.BuildConfig
import com.matko.orb.R
import com.matko.orb.databinding.ActivityMainBinding
import com.matko.orb.model.SearchFilters
import com.matko.orb.util.Constants
import com.matko.orb.viewModel.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    companion object {
        private lateinit var binding: ActivityMainBinding
        fun hideNavBar() {
            binding.navView.isVisible = false
        }
        fun showNavBar() {
            binding.navView.isVisible = true
        }
    }
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var auth: FirebaseAuth
    var backPressedTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE)

        viewModel.searchFilters.value = SearchFilters(
            sharedPreferences.getString(Constants.FILTER_INDUSTRY, "")!!,
            sharedPreferences.getString(Constants.FILTER_COUNTRY, "")!!
        )

        val navView: BottomNavigationView = binding.navView
        navController = findNavController(R.id.fragmentContainerView)
//        navController = Navigation.findNavController(this, R.id.fragmentContainerView)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.listFragment, R.id.favoritesFragment))

//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)

    }

    override fun onResume() {
        super.onResume()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        viewModel.isLoggedIn.value = currentUser != null
        viewModel.currentUser.value = currentUser
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, appBarConfiguration)
    }

//    override fun onBackPressed() {
//        if (backPressedTime + 3000 > System.currentTimeMillis()) {
//            super.onBackPressed()
//            finish()
//        } else {
//            Toast.makeText(this, R.string.exit_app_text, Toast.LENGTH_SHORT).show()
//        }
//        backPressedTime = System.currentTimeMillis()
//    }

}