package com.matko.orb.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.compose.animation.core.updateTransition
import androidx.core.view.isVisible
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.matko.orb.R
import com.matko.orb.databinding.ActivityLoginBinding
import com.matko.orb.util.Constants
import com.matko.orb.util.EmailValidator
import com.matko.orb.util.hideKeyboard

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id_google_sign_in))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

        binding.loginButton.setOnClickListener {
            val email = binding.promptEmail.text.toString()
            val password = binding.promptPassword.text.toString()
            signIn(email, password)
            hideKeyboard()
        }

        binding.signUpButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
            finish()
        }

        binding.googleSignInButton.setOnClickListener {
            signInWithGoogle()
        }

        val isLogOut = intent.getBooleanExtra(Constants.IS_LOGGED_IN, false)
        if (isLogOut){
            logout()
        }
    }

    private fun logout() {
        auth.signOut()
        Toast.makeText(baseContext, R.string.logged_out, Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun signIn(email: String, password: String) {
        binding.loader.isVisible = true
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        binding.loader.isVisible = false
        if (user != null) {
            Toast.makeText(baseContext, resources.getString(R.string.welcome) + user.email, Toast.LENGTH_LONG).show()
            finish()
        } else {
            Toast.makeText(baseContext, R.string.auth_failed, Toast.LENGTH_SHORT).show()
        }
    }

//    [Google sign in]
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == Constants.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(Constants.TAG_GOOGLE_SIGN_IN, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(Constants.TAG_GOOGLE_SIGN_IN, "Google sign in failed", e)
                updateUI(null)
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(Constants.TAG_GOOGLE_SIGN_IN, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(Constants.TAG_GOOGLE_SIGN_IN, "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }
    }

//    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            // There are no request codes
//            val data: Intent? = result.data
//            doSomeOperations()
//        }
//    }

    private fun signInWithGoogle() {
        binding.loader.isVisible = true

        val signInIntent = googleSignInClient.signInIntent
//        resultLauncher.launch(signInIntent, Constants.RC_SIGN_IN)
        startActivityForResult(signInIntent, Constants.RC_SIGN_IN)
    }
    // [END Google sign in]

}