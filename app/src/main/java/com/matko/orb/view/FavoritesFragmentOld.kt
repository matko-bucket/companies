package com.matko.orb.view

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.*
import androidx.fragment.app.Fragment
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.matko.orb.R
import com.matko.orb.databinding.FragmentFavoritesBinding
import com.matko.orb.util.Constants
import com.matko.orb.viewModel.FavoritesRoomDbViewModel
import com.matko.orb.viewModel.MainActivityViewModel

class FavoritesFragmentOld : Fragment() {

//    private val roomDbViewModel: FavoritesRoomDbViewModel by viewModels()
//    private lateinit var binding: FragmentFavoritesBinding
//    private val favoritesListAdapter = FavoritesRecycleViewAdapter(arrayListOf())
//    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setHasOptionsMenu(true)
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        observeViewModel()
//
//        binding.companyFavoritesList.apply {
//            layoutManager = LinearLayoutManager(requireContext())
//            adapter = favoritesListAdapter
//        }
//        roomDbViewModel.fetchFromRoomDB(requireContext())
//
//        binding.searchByName.doAfterTextChanged { editable: Editable? ->
//            roomDbViewModel.searchByName(requireContext(), "%" + editable.toString() + "%")
//        }
//
//    }
//
//    private fun observeViewModel() {
//        roomDbViewModel.companies.observe(viewLifecycleOwner, Observer { companies ->
//            companies?.let {
//                binding.companyFavoritesList.isVisible = it.isNotEmpty()
//                binding.listEmpty.isVisible = it.isEmpty()
//                if (it.isNotEmpty()) {
//                    favoritesListAdapter.updateCompanyList(it)
//                }
//            }
//        })
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.login_signup_menu, menu)
//        mainActivityViewModel.isLoggedIn.observe(viewLifecycleOwner) { isLoggedIn ->
//            menu.getItem(0).isVisible = !isLoggedIn
//            menu.getItem(1).isVisible = isLoggedIn
//        }
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.itemId) {
//            R.id.action_login ->{
////                requireActivity().invalidateOptionsMenu()
//                val intent = Intent(requireContext(), LoginActivity::class.java)
//                startActivity(intent)
//                true
//            }
//            R.id.action_logout -> {
////                requireActivity().invalidateOptionsMenu()
//                val intent = Intent(requireContext(), LoginActivity::class.java).apply {
//                    putExtra(Constants.IS_LOGGED_IN, true)
//                }
//                startActivity(intent)
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//        MainActivity.showNavBar()
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        MainActivity.hideNavBar()
//    }
}