package com.matko.orb.view

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.matko.orb.R
import com.matko.orb.databinding.FragmentListBinding
import com.matko.orb.util.Constants
import com.matko.orb.util.hideKeyboard
import com.matko.orb.viewModel.ListViewModel
import com.matko.orb.viewModel.MainActivityViewModel

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ListViewModel by viewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val companyListAdapter = CompanyListAdapter(arrayListOf())
    private var lastUserInput = ""
    private var country = ""
    private var industry = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        MainActivity.showNavBar()

        binding.companyList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = companyListAdapter
        }

        observeViewModel()

        binding.refreshLayout.setOnRefreshListener {
            hideKeyboard()
            val textInput = binding.searchByName.text.toString()
            if (country.isNullOrEmpty() && industry.isNullOrEmpty() && textInput.isNullOrEmpty()) {
                Toast.makeText(
                    requireContext(),
                    R.string.specify_search_criteria,
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                search(textInput)
            }
            binding.refreshLayout.isRefreshing = false
        }

        binding.filters.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToFiltersFragment()
            Navigation.findNavController(it).navigate(action)
        }

        binding.searchByName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.searchJob?.cancel()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (lastUserInput != p0.toString())
                    search(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) {
                lastUserInput = p0.toString()
            }
        })

    }

    private fun search(companyName: String) {
        viewModel.search(companyName, country, industry)
    }

    private fun observeViewModel() {
        viewModel.companies.observe(viewLifecycleOwner) { companies ->
            companies?.let {
                if (companies.isEmpty()) {
                    binding.companyList.isVisible = false
                    binding.listEmpty.isVisible = true
                } else {
                    binding.companyList.isVisible = true
                    binding.listEmpty.isVisible = false
                    companyListAdapter.updateCompanyList(companies)
                }
                binding.listError.isVisible = false
            }
        }

        viewModel.loadError.observe(viewLifecycleOwner) { isError ->
            isError?.let {
                if (it) {
                    binding.listError.isVisible = true
                    binding.listEmpty.isVisible = false
                } else {
                    binding.listError.isVisible = false
                }
            }
        }

        viewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            isLoading?.let {
                binding.loadingView.isVisible = it
                if (it) {
                    binding.listError.isVisible = false
                    binding.companyList.isVisible = false
                }
            }
        }

        mainActivityViewModel.searchFilters.observe(viewLifecycleOwner) {searchParams ->
            industry = searchParams.industry
            country = searchParams.country
            val textInput = binding.searchByName.text.toString()
                if (industry.isNotEmpty() || country.isNotEmpty()) {
                    binding.filters.setImageResource(R.drawable.ic_baseline_filter_list_teal_35)
                    search(textInput)
                } else {
                    binding.filters.setImageResource(R.drawable.ic_baseline_filter_list_gray_35)
                    if (!textInput.isNullOrEmpty())
                        search(textInput)
                }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.login_signup_menu, menu)
        mainActivityViewModel.isLoggedIn.observe(viewLifecycleOwner) { isLoggedIn ->
            menu.getItem(0).isVisible = !isLoggedIn
            menu.getItem(1).isVisible = isLoggedIn
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_login ->{
//                requireActivity().invalidateOptionsMenu()
                val intent = Intent(requireContext(), LoginActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.action_logout -> {
//                requireActivity().invalidateOptionsMenu()
                val intent = Intent(requireContext(), LoginActivity::class.java).apply {
                    putExtra(Constants.IS_LOGGED_IN, true)
                }
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        MainActivity.showNavBar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MainActivity.hideNavBar()
        _binding = null
    }

}