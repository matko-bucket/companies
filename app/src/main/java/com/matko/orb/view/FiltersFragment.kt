package com.matko.orb.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.matko.orb.R
import com.matko.orb.databinding.FragmentFiltersBinding
import com.matko.orb.model.SearchFilters
import com.matko.orb.util.Constants
import com.matko.orb.viewModel.MainActivityViewModel


class FiltersFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var binding: FragmentFiltersBinding
    private lateinit var spinner: Spinner
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private lateinit var editor: SharedPreferences.Editor
    private var industryItemSelected: String = ""
    private var countrySelected = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFiltersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences: SharedPreferences =
            requireContext().getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()


        mainActivityViewModel.fetchIndustries()

        spinner = binding.industrySpinner
        spinner.onItemSelectedListener = this

        binding.resetFilters.setOnClickListener {
            editor.clear().apply()
            binding.searchByCountry.setText("")
            spinner.setSelection(0)
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        mainActivityViewModel.searchFilters.observe(viewLifecycleOwner) { searchParams ->
            industryItemSelected = searchParams.industry
            countrySelected = searchParams.country
            binding.searchByCountry.setText(countrySelected)
        }

        mainActivityViewModel.industriesList.observe(viewLifecycleOwner) { industriesList ->
            industriesList?.let {
                if (industriesList.isEmpty()) {
                    Toast.makeText(requireContext(), R.string.check_internet_connection, Toast.LENGTH_LONG)
                        .show()
                } else {
                    val list: ArrayList<String> = arrayListOf()
                    list.add(Constants.SELECT_INDUSTRY)
                    for (i in industriesList) {
                        list.add(i.toString())
                    }

                    ArrayAdapter(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        list
                    ).also { adapter ->
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner.adapter = adapter
                    }
                    if (industryItemSelected == "") {
                        spinner.setSelection(0)
                    } else {
                        for (i in 1 until list.size) {
                            if (industryItemSelected == list[i]){
                                spinner.setSelection(i)
                                break
                            }
                        }
                    }
                }
            }
        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val item: String = parent?.getItemAtPosition(position) as String
        if (position > 0) {
            industryItemSelected = item
        } else {
            industryItemSelected = ""
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
//        TO DO("Not yet implemented")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.filters_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.groupId) {
            R.id.action_apply ->{
                applyFilters()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun applyFilters() {
        val action = FiltersFragmentDirections.actionFiltersFragmentToListFragment()
        countrySelected = binding.searchByCountry.text.toString()
        mainActivityViewModel.searchFilters.value = SearchFilters(industryItemSelected, countrySelected)
        editor.putString(Constants.FILTER_COUNTRY, countrySelected).apply()
        editor.putString(Constants.FILTER_INDUSTRY, industryItemSelected).apply()
        view?.let { Navigation.findNavController(it).navigate(action) }
    }
}