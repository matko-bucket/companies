package com.matko.orb.view

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.matko.orb.databinding.FragmentFavoritesRoomDbBinding
import com.matko.orb.viewModel.FavoritesRoomDbViewModel

class FavoritesRoomDbFragment : Fragment() {

    private val roomDbViewModel: FavoritesRoomDbViewModel by viewModels()
    private lateinit var binding: FragmentFavoritesRoomDbBinding
    private val favoritesListAdapter = FavoritesRecycleViewAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesRoomDbBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()

        binding.companyFavoritesList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = favoritesListAdapter
        }
        roomDbViewModel.fetchFromRoomDB(requireContext())

        binding.searchByName.doAfterTextChanged { editable: Editable? ->
            roomDbViewModel.searchByName(requireContext(), "%" + editable.toString() + "%")
        }
    }

    private fun observeViewModel() {
        roomDbViewModel.companies.observe(viewLifecycleOwner, Observer { companies ->
            companies?.let {
                binding.companyFavoritesList.isVisible = it.isNotEmpty()
                binding.listEmpty.isVisible = it.isEmpty()
                if (it.isNotEmpty()) {
                    favoritesListAdapter.updateCompanyList(it, false)
                }
            }
        })
    }


}