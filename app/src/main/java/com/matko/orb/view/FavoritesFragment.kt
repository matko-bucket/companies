package com.matko.orb.view

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.matko.orb.R
import com.matko.orb.databinding.FragmentFavoritesBinding
import com.matko.orb.util.Constants
import com.matko.orb.viewModel.MainActivityViewModel

class FavoritesFragment : Fragment() {

    private lateinit var binding: FragmentFavoritesBinding
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val tabTitle = arrayOf("Room DB", "Firebase")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pager = binding.viewPager2
        val tabLayout = binding.tabLayout
        pager.adapter = FavoritesPagerAdapter(childFragmentManager, lifecycle)

        TabLayoutMediator(tabLayout, pager) {tab , position ->
            tab.text = tabTitle[position]
        }.attach()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.login_signup_menu, menu)
        mainActivityViewModel.isLoggedIn.observe(viewLifecycleOwner) { isLoggedIn ->
            menu.getItem(0).isVisible = !isLoggedIn
            menu.getItem(1).isVisible = isLoggedIn
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_login ->{
//                requireActivity().invalidateOptionsMenu()
                val intent = Intent(requireContext(), LoginActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.action_logout -> {
//                requireActivity().invalidateOptionsMenu()
                val intent = Intent(requireContext(), LoginActivity::class.java).apply {
                    putExtra(Constants.IS_LOGGED_IN, true)
                }
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        MainActivity.showNavBar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MainActivity.hideNavBar()
    }
}