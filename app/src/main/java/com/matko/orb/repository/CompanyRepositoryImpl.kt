package com.matko.orb.repository

import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.matko.orb.api.CompanyApiService
import com.matko.orb.model.*

class CompanyRepositoryImpl(
    private val companyApiService: CompanyApiService,
    private val companyDatabase: CompanyDatabase): CompanyRepository {

    private val firebaseDB = Firebase.firestore
    private lateinit var listenerRegistration: ListenerRegistration

    val companyLoadSuccess = MutableLiveData<CompanyDetail>()
    val companyLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val isCompanyExistingInDatabases = MutableLiveData<Boolean>()
    val commentsLoadSuccess = MutableLiveData<List<Comment>>()
    val savedToFirebaseDb = MutableLiveData<Boolean>()
    val deletedFromFirebaseDb = MutableLiveData<Boolean>()


    override suspend fun getCompanyDetailsFromNetwork(fetchUrl: String): CompanyDetailWrapper {
         return companyApiService.fetchCompanyDetailsV2(fetchUrl)
    }

    override suspend fun getIndustriesFromNetwork(): List<Industries> {
        return companyApiService.fetchIndustries()
    }

    override suspend fun searchCompaniesFromNetwork(
        name: String,
        country: String,
        industry: String
    ): CompanySimpleWrapper {
        return companyApiService.searchCompanies(name, country, industry)
    }

    override suspend fun saveCompanyInRoomDb(company: CompanyDetail): List<Long> {
        return companyDatabase.CompanyDao().insert(company)
    }

    override suspend fun fetchFromRemote(fetchUrl: String): CompanyDetailWrapper {
        return companyApiService.fetchCompanyDetailsV2(fetchUrl)
    }

    override suspend fun deleteCompanyFromRoomDB(orbNumber: String): Int {
        return companyDatabase.CompanyDao().deleteByOrbNumber(orbNumber)
    }

    override suspend fun getCompanyFromRoomDb(orbNumber: String): CompanyDetail? {
        return companyDatabase.CompanyDao().getCompanyDetail(orbNumber)
    }

    override suspend fun getCompanyFromFirebaseDB(userEmail: String?, orbNum: String?) {
        firebaseDB.collection("users/$userEmail/favorite_companies").document("$orbNum")
            .get()
            .addOnSuccessListener {
                val companyDetail = it.toObject<CompanyDetail>()
                companyLoadSuccess.postValue(it.toObject<CompanyDetail>())
                isCompanyExistingInDatabases.postValue(companyDetail != null)
                loading.value = false
            }
            .addOnFailureListener {
                companyLoadError.postValue(true)
                loading.value = false
            }
        getCommentsFromFirebaseDB(orbNum)

    }

    override suspend fun getCommentsFromFirebaseDB(orbNum: String?){
        val commentsList = ArrayList<Comment>()
        listenerRegistration = firebaseDB.collection("comments2")
            .whereEqualTo("orbNumber", orbNum)
            .addSnapshotListener{ value, e ->
                if (e != null)
                    return@addSnapshotListener
                commentsList.clear()
                for (doc in value!!){
                    val comment = doc.toObject<Comment>()
                    commentsList.add(comment)
                }
                commentsLoadSuccess.postValue(commentsList.sortedWith(compareBy { it.date }))
            }
    }

    override suspend fun saveToFirebaseDB(companyDetail: CompanyDetail, currentUser: FirebaseUser?) {
        companyDetail.orbNumber?.let { orb ->
            currentUser?.let { user ->
                user.email?.let { it1 ->
                    firebaseDB.collection("users").document(it1).collection("favorite_companies").document(orb)
                        .set(companyDetail)
                        .addOnSuccessListener {
                            savedToFirebaseDb.postValue(true)
                            isCompanyExistingInDatabases.postValue(true)
                        }
                        .addOnFailureListener {
                            savedToFirebaseDb.postValue(false)
                        }
                }
            }
        }
    }

    override suspend fun deleteFromFirebaseDB(userEmail: String, orbNum: String?) {
        firebaseDB.collection("users/$userEmail/favorite_companies").document("$orbNum")
            .delete()
            .addOnSuccessListener {
                deletedFromFirebaseDb.postValue(true)
                isCompanyExistingInDatabases.postValue(false)
            }
            .addOnFailureListener {
                deletedFromFirebaseDb.postValue(false)
            }
    }

    override fun postComment(comment: Comment) {
        firebaseDB.collection("comments2").document()
            .set(comment)
    }

    fun detachListener(){
        if (this::listenerRegistration.isInitialized)
            listenerRegistration.remove()
    }
}