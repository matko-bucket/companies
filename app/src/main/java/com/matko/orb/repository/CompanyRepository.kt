package com.matko.orb.repository

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentSnapshot
import com.matko.orb.model.*

interface CompanyRepository {

    suspend fun getCompanyDetailsFromNetwork(fetchUrl: String): CompanyDetailWrapper
    suspend fun getIndustriesFromNetwork(): List<Industries>
    suspend fun searchCompaniesFromNetwork(name: String, country: String, industry: String): CompanySimpleWrapper

    suspend fun saveCompanyInRoomDb(company: CompanyDetail): List<Long>
    suspend fun fetchFromRemote(fetchUrl: String): CompanyDetailWrapper
    suspend fun deleteCompanyFromRoomDB(orbNumber: String): Int
    suspend fun getCompanyFromRoomDb(orbNumber: String): CompanyDetail?

    suspend fun getCompanyFromFirebaseDB(userEmail: String?, orbNum: String?)
    suspend fun getCommentsFromFirebaseDB(orbNum: String?)
    suspend fun saveToFirebaseDB(companyDetail: CompanyDetail, currentUser: FirebaseUser?)
    suspend fun deleteFromFirebaseDB(userEmail: String, orbNum: String?)
    fun postComment(comment: Comment)
}